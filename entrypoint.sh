#!/bin/bash
mkdir -p "$MNT_POINT"
echo "$AWS_KEY:$AWS_SECRET_KEY" > /root/passwd && chmod 600 /root/passwd
s3fs "$S3_BUCKET" "$MNT_POINT" -o passwd_file=/root/passwd -o url="$AWS_URL" -o endpoint="$AWS_ENDPOINT" && tail -f /dev/null